Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*DS151 - Desenvolvimento para Dispositivos Móveis*

Prof. Alexander Robert Kutzke

# Atividade Mapas

Crie uma aplicação com o Expo (já inicializada nesse repositório) **que se comunique um [JSON Server](https://github.com/typicode/json-server) para a persistência dos dados**.

A aplicação tem como objetivo a adição de pontos (`Marker`'s) no mapa. Algo como uma aplicação para a adição dos "meus locais favoritos". 

A cada clique longo no mapa o usuário deve ser levado a uma tela diferente onde preencherá dados sobre aquele ponto (título, descrição e qualquer outro dado que considerem
relevante). Ao salvar as alterações (e persistir os dados no JSON Server), a aplicação deve voltar a tela do mapa e apresentar os markers criados.

Ao clicar em um marker existente, a aplicação deve ir para uma tela com detalhes daquele marker e com um opção para removê-lo.

Layout, e demais detalhes são livres. ;)

Pontos extras para quem permitir a adição de imagens para um local criado.
